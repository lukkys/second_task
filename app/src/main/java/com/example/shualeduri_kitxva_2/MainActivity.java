package com.example.shualeduri_kitxva_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private CheckBox box1;
    private CheckBox box2;
    private EditText name;
    private EditText surname;
    private EditText txt;
    private Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        box1 = findViewById(R.id.box1);
        box2 = findViewById(R.id.box2);
        name = findViewById(R.id.name);
        surname = findViewById(R.id.surname);
        txt = findViewById(R.id.text);
        send = findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String box = box1.getText().toString();
                String box1 = box2.getText().toString();
                String name1 = name.getText().toString();
                String surname1 = surname.getText().toString();
                String txt1 = txt.getText().toString();

                if(TextUtils.isEmpty(box1) && TextUtils.isEmpty(box) || name1.length() < 2 || surname1.length() < 3 || txt1.length() < 5){
                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}